# Zearn

## Description

Zearn is a platform allowing trainers/teachers to give training course to students with a system of training sessions, scheduling and marks management.

## Teams members

- Théo MACKOWIAK
- Benjamin DUEZ
- Allan COMMELIN

## What has been done

- Pipelines triggered at change: working test and deployment jobs
- (Facultative feature n°1) PHPLint job : Running several lint processes
- (Facultative feature n°2) Parallelized jobs / job dependencies

## Who worked on what
### On a globalement échangé ensemble sur chacune de nos tâches
- Lint: Benjamin
- PhpUnit: Allan / Théo / Benjamin (On a fait ça ensemble en cours)
- App heroku : Allan / Théo / Benjamin (On a fait ça ensemble en cours)
- Deploiement DPL : Theo
- Optimisation : Allan

## Deployment URLs

- https://test-gitlab-allan.herokuapp.com/
- https://test-gitlab-bduez.herokuapp.com/
- https://test-gitlab-mackowiak.herokuapp.com/

